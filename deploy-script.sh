#!/bin/bash

# Определение переменных из переменных среды GitLab CI
SSH_PRIVATE_KEY="$SSH_PRIVATE_KEY"
SSH_USER="$SSH_USER"

# Установка openssh-client (если еще не установлен)
apk add --no-cache openssh-client

# Инициализация агента SSH
eval $(ssh-agent -s)

# Добавление SSH ключа в агент
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# Создание директории для SSH ключей и установка прав
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Добавление хоста в список известных хостов SSH
ssh-keyscan 157.230.115.210 >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Загрузка и запуск образов на удаленном сервере через SSH
ssh "$SSH_USER@157.230.115.210" "docker pull $DOCKER_USERNAME/backend:latest && docker run -d --name backend -p 8080:8080 $DOCKER_USERNAME/backend:latest"
ssh "$SSH_USER@157.230.115.210" "docker pull $DOCKER_USERNAME/frontend:latest && docker run -d --name frontend -p 3000:3000 $DOCKER_USERNAME/frontend:latest"
